#### Overview
Analytical description of charts is an exciting and important research area with many academia and industry benefits. Yet, this challenging task has received limited attention from the computational linguistics research community. This paper aims to encourage more research into this important area by proposing **[AutoChart](/AutoChart)**, the first large chart analytical description dataset. Specifically, we offer a novel framework that generates the charts and their analytical description automatically. We also empirically demonstrate that the generate analytical descriptions are diverse, coherent, and relevant to the corresponding charts. The image file can be downloaded in [this link](https://drive.google.com/file/d/1SgVqyDnZypO3nSqHAG6aXHal-o-F60EC/view?usp=sharing).

#### Charts and Meta-Information
We generate four types of charts in our dataset: scatter plots, line chart, vertical, and horizontal bar charts.

We preserves the meta-information of the generated charts in JSON files. The meta-information will be used in the analytical description generation module to generate the charts’ corresponding descriptions. Furthermore, the meta-data can be used in evaluating the correctness of future chart understanding models. The charts’ meta-data JSON files will be released together with the charts and analytical descriptions. Specifically, the JSON file captures the following information:

• “model”: A list of dictionaries. The length depended on how many sets of data the chart has. Each dictionary contains the following:
   – “name”: Label of the datapoint.
   – “color” Color corresponding to the ’name’ datapoint in the chart.
   – “coordinates”: Coordinates corresponding to the ’name’ datapoints in the chart. 
   – “x”: x value of the datapoints.
   – “y”: y value of the datapoints.

• “type”: Type of the chart. (scatter plot, line plot, vertical bar chart or horizontal bar chart)

• “general figure info”: A list of dictionaries. Each dictionary contains the following
   – “title”: Text content and bounding box of the title in the chart.
   – “x axis”: Bounding boxes, ticks labels, ticks coordinates, and axis labels corresponding to the x-axis of the chart.
   – “axes info”: Bounding boxes corresponding to the axes of the chart.
   – “y axis”: Bounding boxes, ticks labels, ticks coordinates, and axis labels corresponding to the
y-axis of the chart.
   – “legend”: Text content and bounding box of the legend in the chart.

• “marker”: Marker of each scatter chart. This key is not in the line charts and bar charts. 

• “image index”: Index of each chart

#### Auto Chart Examples

**Bar Chart Example:**

<img src="/Example/bar_example.png"  width="500">

Generated corresponding text: *This bar chart shows the number of visits to the United States of America (USA) by overseas residents from 1950 to 2010. Each bar represents the number of visits in the unit of million. In 1950, the number of visits is about 60 million. Overall, the numbers of visits seem to be relatively stable, without noticeable changes in 1960, 1970, and 1980. A small decrease in the number of visits occurred 1990, showing about 56 million visits. After 1990, more decreases are recorded in 2000 and 2010, respectively.In 2000, the number of visits dropped to about 50 million. The lowest number of visits is recorded in 2010, which sees a sharp drop of about 40 million visits and ends with about 8 million.More analysis on the possible factors behind the trend may be needed to understand the current situation in the the USA.*

**Line Chart Example:**

<img src="/Example/line_example.png"  width="500">

Generated corresponding text: *The line graph displays the number of consumption of fast food (hamburger) in Canada and the USA, respectively, from 2013 through 2019. In this chart, the unit of measurement is Local Currency, as seen on the y-axis. The data related to Canada is rendered yellow and the cyan line is for the USA. It is obvious that both countries shared similar increasing trends in the number of consumption in the past 6 years.For Canada, by 2013 the number of consumption reached nearly 12, while the number continued to increase until 34 in 2019.And for the USA, in 2013, the number of consumption was about 26, after that, each year has witnessed some increase. In 2019, the number of consumption was about 42. In the past 6 years, the USA had consistently more than Canada. With 13 for Canada and 35 for the USA in 2017, a new record of large difference was registered.It would be interesting to see what would happen in the next decade in these two countries in terms of current situations.*

#### Citation
```
@inproceedings{autochart21,
    title={AutoChart: A Dataset for Chart-to-Text Generation Task},
    author={Zhu, Jiawen and Ran, Jinye, and Lee, Roy Ka-Wei and Choo, Tse Wei Kenny and Li, Zhi},
    booktitle={Recent Advances in Natural Language Processing},
    year={2021},
    organization={ACL}
}
```
