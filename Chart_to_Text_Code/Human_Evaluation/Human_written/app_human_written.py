from flask import *
import hashlib
from dbInit import *
from requests import post
import time
# from datetime import datetime
# app = Flask(__name__)
# sha256
# sh = hashlib.sha256()
# sh.update('abcdef'.encode('utf-8'))
# return(sh.hexdigest())
class ReCaptha():
    # self generated
    SECRET_KEY = 'verylongstringhasgwhatever' 
    RECAPTCHA_USE_SSL= False
    RECAPTCHA_PUBLIC_KEY = '6LduXd8ZAAAAAMYai16rNNpXLONV642av1M8AOfu' # google generated
    RECAPTCHA_PRIVATE_KEY = '6LduXd8ZAAAAAOAHlt8ULNiBtlZ3BsRl5obr8vTl' # google generated
    RECAPTCHA_DATA_ATTRS = {'theme': 'light'}


def is_human(captcha_response):
    payload = {'response': captcha_response, 'secret': private_key}
    response = post("https://www.google.com/recaptcha/api/siteverify", data=payload)
    response_text = json.loads(response.text)
    return response_text['success']

app.config["SECRET_KEY"]="fcd51a010960b335dec6e0b1ec57368d591db6b4d8f33086a23c80e4c1ca0994"
app.config.from_object(ReCaptha)

pub_key = ReCaptha.RECAPTCHA_PUBLIC_KEY
private_key = ReCaptha.RECAPTCHA_PRIVATE_KEY
# recaptcha.init_app(app, "6LduXd8ZAAAAAMYai16rNNpXLONV642av1M8AOfu", "6LduXd8ZAAAAAOAHlt8ULNiBtlZ3BsRl5obr8vTl", is_enabled=True)
@app.route('/', methods=["GET"])
def index():
	# confirmCode=request.args.get('c')
	# lst=['123','321']
	s = 0
	return render_template('index.html', page = s)
	# if confirmCode in lst:
	# 	s = 0
	# 	return render_template('index.html', page = s)
	# else:
	# 	s = 404
	# 	return render_template('index.html', page = s)

# @app.route('/contactInfo', methods=["POST"])
# def contact():
# 	# pid_p = tblChart.query.order_by(tblChart.completionCode).first().pid
# 	pid_p = tblStatus.query.filter_by(status=0).first().pid
# 	# pid_p = request.form.get('pid')
# 	s = 4
# 	# session['pid'] = pid_p
# 	return render_template('index.html', page = s, pid = pid_p)

@app.route('/demo', methods=["POST"])
def demo():
	if 'code2' in session:
		return render_template('index.html', page=3, code=session['code2'])
	if 'demo2' in session:
		pid_p = session['pid2']
		chart = tblChart3.query.filter_by(pid=pid_p).first()
		return render_template('index.html', page = s, description=chart.description, img=chart.path, pid=pid_p, pub_key=pub_key, ques=chart.question)
	s=1
	tblChart3.query.order_by()
	if 'pid2' in session:
		pid_p = session['pid2']
	else:
		res = tblChart3.query.filter_by(status=0).first()
		if res == None:
			return render_template('index.html', page = 404)
		session['pid2'] = res.pid
		pid_p = session['pid2']
		chart = tblChart3.query.filter_by(pid=pid_p).update({'status':1})
		db.session.commit()
	# contactinfo_p = request.form.get('contactInfo')
	
	# ipAddr = request.remote_addr

	return render_template('index.html', page = s, pid = pid_p)

@app.route('/survey', methods=["POST"])
def survey():
	pid_p = request.form.get('pid2')
	if('demo2' not in session):
		# sex=request.form.get('Sex')
		# if(sex=='other'):
		# 	sex=request.form.get('Sex0')
		# race=request.form.get('Race')
		# if(race=='other'):
		# 	race=request.form.get('Race0')
		lan=request.form.get('Lan')
		if(lan=='other'):
			lan=request.form.get('Lan0')
		edu=request.form.get('Edu')
		if(edu=='other'):
			edu=request.form.get('Edu0')
		age=request.form.get('Age')
		demo = tblChart3.query.filter_by(pid=pid_p).update({'language':lan, 'age':age, 'edu':edu, 'status':2})
		# demo = tblChart2(pid=pid_p, language=lan, age=age, edu=edu)
		# db.session.add(demo)
		db.session.commit()
		session['demo2'] = 'done'
	s = 2
	chart = tblChart3.query.filter_by(pid=pid_p).first()
	# path='chart1.png'
	# des="This bar chart shows the number of visits to the United States of America (USA) by overseas residents from 1950 to 2010. Each bar represents the number of visits in the unit of million. In 1950, the number of visits is about 60 million."
	return render_template('index.html', page = s, description=chart.description, img=chart.path, pid=pid_p, pub_key=pub_key, ques=chart.question)

@app.route('/submit', methods=["POST"])
def submit():
	if 'code2' in session:
		return render_template('index.html', page=3, code=session['code2'])
	else:
		if request.method == 'POST':
			captcha_response = request.form['g-recaptcha-response']
			if is_human(captcha_response):
				info=request.form.get('INFO')
				natural=request.form.get('NATU')
				qua=request.form.get('QUA')
				pid_p = request.form.get('pid')
				answer = request.form.get('answer')
				# score=tblChart2(pid=pid_p, informativeness=info, naturalness=natural, quality=qua, ans=answer)
				# d_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
				chart = tblChart3.query.filter_by(pid=pid_p).update({'status':2, 'informativeness':info, 'naturalness':natural, 'quality':qua, 'ans':answer})
				# db.session.add(score)
				db.session.commit()
				code = tblChart3.query.filter_by(pid=pid_p).first().completionCode
				session['code2'] = code
				return render_template('index.html', page=3,code = code)
			else:
				pid_p = request.form.get('pid')
				chart = tblChart3.query.filter_by(pid=pid_p).first()
				return render_template('index.html', page=2, description=chart.description, img=chart.path, pid=pid_p, pub_key=pub_key)
			return return render_template('index.html', page=3,code = code)
		

	# pid=request.form.get('pid')
	# s = 3
	# scoreInfo = tblScore.query.filter_by(pid=pid_p).first().update({'info':INFO, 'natu':NATU, 'qua':QUA})
	
	#after deploying in server, $cplCode shuld be get from database
	# sh = hashlib.sha256()
	# sh.update('str(datetime.now())'.encode('utf-8'))
	# cplCode=sh.hexdigest()

	# return render_template('index.html', page = s, code = code)

if __name__ == "__main__":
	# app.run(debug = True)
	app.run(host='0.0.0.0',port=5000, debug=True)