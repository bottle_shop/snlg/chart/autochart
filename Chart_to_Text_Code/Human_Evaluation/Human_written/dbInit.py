from flask_sqlalchemy import SQLAlchemy
from flask import *
import pymysql

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"]='mysql+pymysql://doadmin:is4ms448qgbyb4g7@chartsurveydb-do-user-8217338-0.b.db.ondigitalocean.com:25060/defaultdb'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"]=False

db=SQLAlchemy(app)

class tblChart(db.Model):
	__tablename__ = "tblChart"
	pid = db.Column(db.String(15), primary_key=True)
	description = db.Column(db.String(3000), nullable=False)
	path = db.Column(db.String(30), nullable=False)
	completionCode = db.Column(db.String(256), nullable = False)
	question = db.Column(db.String(300), nullable = False)


class tblStatus(db.Model):
	__tablename__ = "tblStatus"
	pid = db.Column(db.String(15), primary_key=True)
	contactInfo = db.Column(db.String(200))
	time = db.Column(db.Time)
	status = db.Column(db.Integer, nullable=False)

class tblDemo(db.Model):
	__tablename__ = "tblDemo"
	pid = db.Column(db.String(15), primary_key=True)
	gender = db.Column(db.String(10))
	# race = db.Column(db.String(50))
	language = db.Column(db.String(50))
	edu = db.Column(db.String(50))
	age = db.Column(db.String(50))

class tblScore(db.Model):
	__tablename__ = "tblScore"
	pid = db.Column(db.String(15), primary_key=True)
	naturalness = db.Column(db.Integer, nullable=False)
	informativeness = db.Column(db.Integer, nullable=False)
	quality = db.Column(db.Integer, nullable=False)
	ans = db.Column(db.String(100), primary_key=True)
	
class tblChart2(db.Model):
	__tablename__ = "tblChart2"
	pid = db.Column(db.String(15), primary_key=True)
	description = db.Column(db.String(3000), nullable=False)
	path = db.Column(db.String(30), nullable=False)
	question = db.Column(db.String(300), nullable = False)
	completionCode = db.Column(db.String(256), nullable = False)
	status = db.Column(db.Integer, nullable=False)
	language = db.Column(db.String(50))
	edu = db.Column(db.String(50))
	age = db.Column(db.String(50))
	naturalness = db.Column(db.Integer)
	informativeness = db.Column(db.Integer)
	quality = db.Column(db.Integer)
	ans = db.Column(db.String(100))
	tag = db.Column(db.String(10))

class tblChart3(db.Model):
	__tablename__ = "tblChart3"
	pid = db.Column(db.String(15), primary_key=True)
	description = db.Column(db.String(3000), nullable=False)
	path = db.Column(db.String(30), nullable=False)
	question = db.Column(db.String(300), nullable = False)
	completionCode = db.Column(db.String(256), nullable = False)
	status = db.Column(db.Integer, nullable=False)
	language = db.Column(db.String(50))
	edu = db.Column(db.String(50))
	age = db.Column(db.String(50))
	naturalness = db.Column(db.Integer)
	informativeness = db.Column(db.Integer)
	quality = db.Column(db.Integer)
	ans = db.Column(db.String(100))
	tag = db.Column(db.String(10))

# if __name__ == '__main__':
# 	db.drop_all()
# 	db.create_all()