from flask import *
import hashlib
from dbInit import *
# from datetime import datetime
app = Flask(__name__)

# sha256
# sh = hashlib.sha256()
# sh.update('abcdef'.encode('utf-8'))
# return(sh.hexdigest())

@app.route('/', methods=["GET"])
def index():
	confirmCode=request.args.get('c')
	chart = tblChart.query.filter_by(link=confirmCode).first()
	if chart != None && chart.status==0:
		s = 0
		return render_template('index.html', page = s, pid = chart.pid)
	else:
		s = 404
		return render_template('index.html', page = s)

@app.route('/contactInfo', methods=["POST"])
def contact():
	pid_p = request.form.get('pid')
	s = 4
	return render_template('index.html', page = s, pid = pid_p)

@app.route('/demo', methods=["POST"])
def demo():
	s=1
	pid_p = request.form.get('pid')
	contactinfo_p = request.form.get('contactInfo')
	chart = tblChart.query.filter_by(pid=pid_p).first().update({'status':1, 'contactInfo':contactinfo_p})
	# ipAddr = request.remote_addr

	return render_template('index.html', page = s, pid = pid_p)

@app.route('/survey', methods=["POST"])
def survey():
	sex=request.form.get('Sex')
	if(sex=='other'):
		sex=request.form.get('Sex0')
	race=request.form.get('Race')
	if(race=='other'):
		race=request.form.get('Race0')
	lan=request.form.get('Lan')
	if(lan=='other'):
		lan=request.form.get('Lan0')
	edu=request.form.get('Edu')
	if(edu=='other'):
		edu=request.form.get('Edu0')
	age=request.form.get('Age')

	pid_p = request.form.get('pid')

	demoInfo = tblDemo.query.filter_by(pid=pid_p).first().update({'gender':sex, 'race':race, 'language':lan, 'age':age, 'edu':edu})
	chart = tblChart.query.filter_by(pid=pid_p).first()
	# path='chart1.png'
	# des="This bar chart shows the number of visits to the United States of America (USA) by overseas residents from 1950 to 2010. Each bar represents the number of visits in the unit of million. In 1950, the number of visits is about 60 million."
	return render_template('index.html', page = s, description=chart.description, img=chart.path, pid=pid_p)

@app.route('/submit', methods=["POST"])
def submit():
	info=request.form.get('INFO')
	natural=request.form.get('NATU')
	qua=request.form.get('QUA')
	pid=request.form.get('pid')
	s = 3

	pid_p = request.form.get('pid')
	
	scoreInfo = tblScore.query.filter_by(pid=pid_p).first().update({'info':INFO, 'natu':NATU, 'qua':QUA})
	chart = tblChart.query.filter_by(pid=pid_p)
	#after deploying in server, $cplCode shuld be get from database
	# sh = hashlib.sha256()
	# sh.update('str(datetime.now())'.encode('utf-8'))
	# cplCode=sh.hexdigest()

	return render_template('index.html', page = s, code = chart.completionCode)

if __name__ == "__main__":
	app.run(debug = True)
